<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP & MySQL 😖</title>
</head>

<body>
    <?php


    $dsn = 'mysql:host=localhost;dbname=colyseum;port=3306;charset=utf8';
    $pdo = new PDO($dsn, 'root', 'pt12MERD');
    // Exercice 1
    // Afficher tous les clients.
    echo "<h3>Afficher tous les clients.</h3>";
    $query = $pdo->query("SELECT * FROM clients");

    $resultat = $query->fetchAll();
    echo "<table border=\"1\">";
    foreach ($resultat as $key => $value) {
        echo "<tr>";
        echo "<td>" . $resultat[$key]['id'] . "</td>";
        echo "<td>" . $resultat[$key]['lastName'] . "</td>";
        echo "<td>" . $resultat[$key]['firstName'] . "</td>";
        echo "<td>" . $resultat[$key]['birthDate'] . "</td>";
        echo "<td>" . $resultat[$key]['card'] . "</td>";
        echo "<td>" . $resultat[$key]['cardNumber'] . "</td>";
        // echo "<td>".$resultat[$key]['']."</td>";
        echo "</tr>";
    }
    echo "</table>";

    // Exercice 2

    // Afficher tous les types de spectacles possibles.
    echo "<h3>Afficher tous les types de spectacles possibles.</h3>";

    $query = $pdo->query("SELECT * FROM showTypes");

    $resultat = $query->fetchAll();
    echo "<table border=\"1\">";
    foreach ($resultat as $key => $value) {
        echo "<tr>";
        echo "<td>" . $resultat[$key]['id'] . "</td>";
        echo "<td>" . $resultat[$key]['type'] . "</td>";
        echo "</tr>";
    }
    echo "</table>";


    // Exercice 3

    // Afficher les 20 premiers clients.

    echo "<h3>Afficher les 20 premiers clients.</h3>";

    $query = $pdo->query("SELECT * FROM clients WHERE id <= 20");

    $resultat = $query->fetchAll();
    echo "<table border=\"1\">";
    foreach ($resultat as $key => $value) {
        echo "<tr>";
        echo "<td>" . $resultat[$key]['id'] . "</td>";
        echo "<td>" . $resultat[$key]['lastName'] . "</td>";
        echo "<td>" . $resultat[$key]['firstName'] . "</td>";
        echo "</tr>";
    }
    echo "</table>";



    // Exercice 4

    // N'afficher que les clients possédant une carte de fidélité.

    echo "<h3>N'afficher que les clients possédant une carte de fidélité.</h3>";

    $query = $pdo->query("SELECT * FROM clients WHERE card > 0");

    $resultat = $query->fetchAll();
    echo "<table border=\"1\">";
    foreach ($resultat as $key => $value) {
        echo "<tr>";
        echo "<td>" . $resultat[$key]['id'] . "</td>";
        echo "<td>" . $resultat[$key]['lastName'] . "</td>";
        echo "<td>" . $resultat[$key]['firstName'] . "</td>";
        echo "<td>" . $resultat[$key]['card'] . "</td>";
        echo "</tr>";
    }
    echo "</table>";


    // Exercice 5

    // Afficher uniquement le nom et le prénom de tous les clients dont le nom commence par la lettre "M".
    // Les afficher comme ceci :

    // Nom : *Nom du client*
    // Prénom : *Prénom du client*

    // Trier les noms par ordre alphabétique.

    echo "<h3>Afficher uniquement le nom et le prénom de tous les clients dont le nom commence par la lettre \"M\".</h3>";
    echo "<h5>Les afficher comme ceci :<br><br>Nom : *Nom du client*<br>Prénom : *Prénom du client*<br><br>Trier les noms par ordre alphabétique.</h5>";

    $query = $pdo->query("SELECT * FROM clients WHERE lastName LIKE 'm%' > 0 ORDER BY lastName");

    $resultat = $query->fetchAll();

    foreach ($resultat as $key => $value) {
        echo "Nom : " . $resultat[$key]['lastName'] . "<br>";
        echo "Prénom : " . $resultat[$key]['firstName'] . "<br><br>";
    }

    // Exercice 6

    // Afficher le titre de tous les spectacles ainsi que l'artiste, la date et l'heure. Trier les titres par ordre alphabétique. Afficher les résultat comme ceci : *Spectacle* par *artiste*, le *date* à *heure*.

    echo "<h3>Afficher le titre de tous les spectacles ainsi que l'artiste, la date et l\'heure. Trier les titres par ordre alphabétique.</h3>";
    echo "<h5>Afficher les résultat comme ceci : *Spectacle* par *artiste*, le *date* à *heure*.</h5>";

    $query = $pdo->query("SELECT * FROM shows");

    $resultat = $query->fetchAll();

    foreach ($resultat as $key => $value) {

        echo $resultat[$key]['title'] . " par " . $resultat[$key]['performer'] . ", le " . $resultat[$key]['date'] . " à " . $resultat[$key]['startTime'] . ".<br>";
    };


    // Exercice 7

    // Afficher tous les clients comme ceci :
    // Nom : *Nom de famille du client*
    // Prénom : *Prénom du client*
    // Date de naissance : *Date de naissance du client*
    // Carte de fidélité : *Oui (Si le client en possède une) ou Non (s'il n'en possède pas)*
    // Numéro de carte : *Numéro de la carte fidélité du client s'il en possède une.*

    echo "<h3>Afficher tous les clients comme ceci :</h3>";
    echo "<h5>Nom : *Nom de famille du client*<br>Prénom : *Prénom du client*<br>Date de naissance : *Date de naissance du client*<br>Carte de fidélité : *Oui (Si le client en possède une) ou Non (s'il n'en possède pas)*<br>Numéro de carte : *Numéro de la carte fidélité du client s'il en possède une.*</h5>";

    $query = $pdo->query("SELECT * FROM clients");

    $resultat = $query->fetchAll();

    foreach ($resultat as $key => $value) {

        echo "Nom : " . $resultat[$key]['lastName'] . "<br>Prénom : " . $resultat[$key]['firstName'] . "<br>Date de naissance : " . $resultat[$key]['birthDate'];
        if ($resultat[$key]['card'] == 0) {
            echo "<br>Carte de fidélité : Non<br><hr>";
        } else {
            echo "<br>Carte de fidélité : Oui<br>Numéro de carte : " . $resultat[$key]['cardNumber'] . "<br><hr>";
        }
    };
?>

</body>

</html>